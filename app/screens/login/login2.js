import React from 'react';
import {
  View,
  Image,
  Keyboard,
  ActivityIndicator
} from 'react-native';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkTheme,
  RkStyleSheet,
} from 'react-native-ui-kitten';
import { FontAwesome } from '../../assets/icons';
import { GradientButton } from '../../components/gradientButton';
import { scaleVertical } from '../../utils/scale';
import NavigationType from '../../config/navigation/propTypes';
import { APPConstant } from '../../utils/appConstant';

export class LoginV2 extends React.Component {

  constructor(props) {
    super(props)

    this.state = {
      username: "",
      password: "",
      error: "",
      showProgress: false,
    }
  }

  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    header: null,
  };

  onLoginButtonPressed = () => {
    try {
      fetch(APPConstant.BASE_URL + '/auth', {
        method: 'POST',
        headers: {
          'Accept': 'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          UserId: this.state.username,
          Password: this.state.password
        })
      }).then(response =>{
        console.warn(response.statusText);
        alert()
        // alert(response.ok())
      })
        // .then((responseJson) => {
        //   alert(responseJson);
        // })
        .catch(error => {
         // alert(error.message);
          console.error(error);
        });
    } catch (error) {
      this.setState({ error: error });
      console.log("error " + error);
      // this.setState({ showProgress: false });
    }

    // this.props.navigation.goBack();
  };

  onSignUpButtonPressed = () => {
    this.props.navigation.navigate('SignUp');
  };

  getThemeImageSource = (theme) => (
    theme.name === 'light' ?
      require('../../assets/images/logo_11.png') : require('../../assets/images/Logos/l8.png')
  );

  renderImage = () => (
    <Image style={styles.image} source={this.getThemeImageSource(RkTheme.current)} />
  );

  render = () => (
    <RkAvoidKeyboard
      style={styles.screen}
      onStartShouldSetResponder={() => true}
      onResponderRelease={() => Keyboard.dismiss()}>
      <View style={styles.header}>
        {this.renderImage()}
        <RkText rkType='logo h0'>Login here</RkText>
        <RkText rkType='primary3' style={styles.error}>
          {this.state.error}
        </RkText>
      </View>
      <View style={styles.content}>
        <View>
          <RkTextInput rkType='rounded' placeholder='Username' onChangeText={(text) => this.setState({ username: text })} />
          <RkTextInput rkType='rounded' placeholder='Password' secureTextEntry onChangeText={(text) => this.setState({ password: text })} />
          <GradientButton
            style={styles.save}
            rkType='large'
            text='LOGIN'
            onPress={this.onLoginButtonPressed}
          />

        </View>

        <View style={styles.footer}>
          <View style={styles.textRow}>
            <RkText rkType='primary3'>Don’t have an account?</RkText>
            <RkButton rkType='clear' onPress={this.onSignUpButtonPressed}>
              <RkText rkType='header6'>Sign up now</RkText>
            </RkButton>
          </View>
        </View>
      </View>
    </RkAvoidKeyboard>

  );
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    padding: scaleVertical(16),
    flex: 1,
    justifyContent: 'space-between',
    backgroundColor: theme.colors.screen.base,
  },
  image: {
    height: scaleVertical(177),
    resizeMode: 'contain',
  },
  header: {
    // paddingBottom: scaleVertical(10),
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  content: {
    justifyContent: 'space-between',
    flex: 1
  },
  save: {
    marginVertical: 20,
  },
  buttons: {
    flexDirection: 'row',
    marginBottom: scaleVertical(24),
    marginHorizontal: 24,
    justifyContent: 'space-around',
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  button: {
    borderColor: theme.colors.border.solid,
  },
  footer: {},
  loader: {
    marginTop: 20
  },
  error: {
    color: 'red',
    paddingTop: 10
  },
}));
