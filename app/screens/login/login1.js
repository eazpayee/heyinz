import React from 'react';
import {
  View,
  Image,
  Dimensions,
  Keyboard,
  KeyboardAvoidingView,
  Text, TextInput, Button
} from 'react-native';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkAvoidKeyboard,
  RkStyleSheet,
  RkTheme,
} from 'react-native-ui-kitten';
import { FontAwesome } from '../../assets/icons';
import { GradientButton } from '../../components/gradientButton';
import { scaleModerate, scaleVertical,scale } from '../../utils/scale';
import NavigationType from '../../config/navigation/propTypes';



export class LoginV1 extends React.Component {
  static propTypes = {
    navigation: NavigationType.isRequired,
  };
  static navigationOptions = {
    header: null,
  };

  // getThemeImageSource = (theme) => (
  //   theme.name === 'light' ?
  //     require('../../assets/images/backgroundLoginV1.png') : require('../../assets/images/backgroundLoginV1DarkTheme.png')
  // );


  renderImage = () => {
    const screenSize = Dimensions.get('window');
    const imageSize = {
      width: Math.round(screenSize.width / 2),
      height: Math.round(screenSize.height - scaleModerate(450, 1)),
    };
    return (
      <Image
        style={[styles.image, imageSize]}
        source={require('../../assets/images/logo_main1.png')}
      />
    );
  };

  onLoginButtonPressed = () => {
    this.props.navigation.goBack();
  };

  onSignUpButtonPressed = () => {
    this.props.navigation.navigate('SignUp');
  };

  render = () => (
    <RkAvoidKeyboard

      onStartShouldSetResponder={() => true}
      onResponderRelease={() => Keyboard.dismiss()}
      style={styles.screen} enabled >
      {this.renderImage()}
      <View style={styles.content}>
      <View>
        <RkTextInput rkType='rounded' placeholder='Username' />
        <RkTextInput rkType='rounded' placeholder='Password' secureTextEntry />
        <GradientButton
          style={styles.save}
          rkType='large'
          text='LOGIN'
          onPress={this.onLoginButtonPressed}
        />
      </View>
      <View style={styles.buttons}>
        <RkButton style={styles.button} rkType='social'>
          <RkText rkType='awesome hero'>{FontAwesome.twitter}</RkText>
        </RkButton>
        <RkButton style={styles.button} rkType='social'>
          <RkText rkType='awesome hero'>{FontAwesome.google}</RkText>
        </RkButton>
        <RkButton style={styles.button} rkType='social'>
          <RkText rkType='awesome hero'>{FontAwesome.facebook}</RkText>
        </RkButton>
      </View>
      <View style={styles.footer}>
        <View style={styles.textRow}>
          <RkText rkType='primary3'>Don’t have an account?</RkText>
          <RkButton rkType='clear' onPress={this.onSignUpButtonPressed}>
            <RkText rkType='header6'>Sign up now</RkText>
          </RkButton>
        </View>
      </View>
    </View>
    </RkAvoidKeyboard>
  )
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: theme.colors.screen.base,
  },
  image: {
    marginTop: scaleModerate(65, 1),
    resizeMode: 'cover',
    marginBottom: scaleVertical(40),
  },
  container: {
    paddingHorizontal: 17,
    paddingBottom: scaleVertical(22),
    alignItems: 'center',
    flex: 1,
    // marginTop:scaleVertical(100)
  },
  footer: {
    justifyContent: 'flex-end',
    flex: 2,
    marginBottom: scaleVertical(150)
  },
  buttons: {
    flexDirection: 'row',
    //marginBottom: scaleVertical(24),
  },
  button: {
    marginHorizontal: 14,
  },
  save: {
    // flex: 2,
    // alignItems: 'center',
    // justifyContent: 'center',        
    // //backgroundColor: '#012160',
    // colors: theme.colors.screen.base,
    // height: scaleVertical(56),
    // borderRadius: 28,
    // width: scale(250)
    marginVertical: 20,
  },
  textRow: {
    justifyContent: 'center',
    flexDirection: 'row',
  },
}));
