import React from 'react';
import {
  View,
  Image,
  Keyboard,
} from 'react-native';
import {
  RkButton,
  RkText,
  RkTextInput,
  RkStyleSheet,
  RkTheme,
  RkAvoidKeyboard,
} from 'react-native-ui-kitten';
import { GradientButton } from '../../components/';
import { scaleVertical } from '../../utils/scale';
import NavigationType from '../../config/navigation/propTypes';
import { PayuMoney } from 'react-native-payumoney';
import { APPConstant } from '../../utils/appConstant';

export class PaywithPayUMoney extends React.Component {
  static navigationOptions = {
    header: null,
  };
  static propTypes = {
    navigation: NavigationType.isRequired,
  };

  getThemeImageSource = (theme) => (
    theme.name === 'light' ?
      require('../../assets/images/logo.png') : require('../../assets/images/logoDark.png')
  );

  renderImage = () => (
    <Image style={styles.image} source={this.getThemeImageSource(RkTheme.current)} />
  );

  onSignUpButtonPressed = () => {

    let amount = 500;
    let txid = new Date().getTime() + "";
    let productId = "product101";
    let name = "asdf";
    let email = "hello@world.com";
    let phone = "1231231231";
    let surl = "http://api.heyinz.in//api/v1/auth"; //can be diffrennt for Succes
    let furl = "http://api.heyinz.in//api/v1/auth"; //can be diffrennt for Failed
    let id = 5617330; //Your Merchant ID here
    let key = "RBSleVKb"; //Your Key Here
    let sandbox = true; //Make sure to set false on production or you will get error
    fetch(APPConstant.BASE_URL + '/get-hash-payumoney', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        key: key,
        txnid: txid,
        amount: amount,
        productinfo: productId,
        firstname: name,
        email: email
      }),
    })
      .then((response) => response.text()
      )
      .then((hash) => {
        //console.warn("hash" + hash);
        let options = {
          amount: amount,
          txid: txid,
          productId: productId,
          name: name,
          email: email,
          phone: phone,
          id: id,
          key: key,
          surl: surl,
          furl: furl,
          sandbox: sandbox,
          hash: hash
        };
        console.warn(options);
        PayuMoney.pay(options).then((d) => {
          console.warn("suc:" + d); // WIll get a Success response with verification hash
        }).catch(function (error) {
          console.log('There has been a problem with your fetch operation: ' + error.message);
          // ADD THIS THROW error
          throw error;
        });
      })

    //this.props.navigation.goBack();
  };

  onSignInButtonPressed = () => {
    this.props.navigation.navigate('Login1');
  };

  render = () => (
    <RkAvoidKeyboard
      style={styles.screen}
      onStartShouldSetResponder={() => true}
      onResponderRelease={() => Keyboard.dismiss()}>
      <View style={{ alignItems: 'center' }}>
        {this.renderImage()}
        <RkText rkType='h1'>Payment</RkText>
      </View>
      <View style={styles.content}>
        <View>
          <RkTextInput rkType='rounded' placeholder='Name' />
          <RkTextInput rkType='rounded' placeholder='Email' />
          <RkTextInput rkType='rounded' placeholder='Password' secureTextEntry />
          <RkTextInput rkType='rounded' placeholder='Confirm Password' secureTextEntry />
          <GradientButton
            style={styles.save}
            rkType='large'
            text='SIGN UP'
            onPress={this.onSignUpButtonPressed}
          />
        </View>
        <View style={styles.footer}>
          <View style={styles.textRow}>
            <RkText rkType='primary3'>Already have an account?</RkText>
            <RkButton rkType='clear' onPress={this.onSignInButtonPressed}>
              <RkText rkType='header6'>Sign in now</RkText>
            </RkButton>
          </View>
        </View>
      </View>
    </RkAvoidKeyboard>
  )
}

const styles = RkStyleSheet.create(theme => ({
  screen: {
    padding: 16,
    flex: 1,
    justifyContent: 'space-around',
    backgroundColor: theme.colors.screen.base,
  },
  image: {
    marginBottom: 10,
    height: scaleVertical(77),
    resizeMode: 'contain',
  },
  content: {
    justifyContent: 'space-between',
  },
  save: {
    marginVertical: 20,
  },
  buttons: {
    flexDirection: 'row',
    marginBottom: 24,
    marginHorizontal: 24,
    justifyContent: 'space-around',
  },
  footer: {
    justifyContent: 'flex-end',
  },
  textRow: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
}));
