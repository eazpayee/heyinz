import { scaleVertical } from '../../utils/scale';

export const GradientButtonTypes = (theme) => ({
  _base: {
    button: {
      alignItems: 'stretch',
      paddingVertical: 0,
      paddingHorizontal: 0,
      height: scaleVertical(40),
      borderRadius: 20,
      backgroundColor: '#012160'
    },
    gradient: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      borderRadius: 20,
      colors: theme.colors.gradients.base,   
      backgroundColor: '#012160'   
    },
    text: {
      backgroundColor: 'transparent',
      color: theme.colors.text.inverse,
    },
  },
  large: {
    button: {
      alignSelf: 'stretch',
      height: scaleVertical(56),
      borderRadius: 28,
      backgroundColor: '#012160'
    },
    gradient: {
      borderRadius: 28,
      backgroundColor: '#012160'
    },
  },
  statItem: {
    button: {
      flex: 1,
      borderRadius: 5,
      marginHorizontal: 10,
      height: null,
      alignSelf: 'auto',
      backgroundColor: '#012160'
    },
    gradient: {
      flex: 1,
      borderRadius: 5,
      padding: 10,
      backgroundColor: '#012160'
    },
  },
});
